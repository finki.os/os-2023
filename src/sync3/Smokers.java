package sync3;

import java.util.concurrent.Semaphore;

public class Smokers {

    static Semaphore accessTable;
    static Semaphore emptyTable;
    static Semaphore wait[];
    static boolean waiting[];

    static void init() {
        accessTable = new Semaphore(0);
        emptyTable = new Semaphore(1);
        wait = new Semaphore[3];
        for (int i = 0; i < 3; i++) {
            wait[i] = new Semaphore(0);
        }
        waiting = new boolean[3];
    }

    public static class Table {

        private int type;

        public void putItems() {
            this.type = (int) Math.round(Math.random() * 3);
        }

        public void consume(int type) {
            System.out.println("Consuming...");
        }

        public boolean hasMyItems(int type) {
            return (this.type == type);
        }
    }

    public static class Agent extends Thread {

        private Table table;

        public Agent(Table table) {
            this.table = table;
        }

        public void execute() throws InterruptedException {
            emptyTable.acquire();
            this.table.putItems();

            for (int i = 0; i < 3; i++) {
                if (waiting[i]) {
                    waiting[i] = false;
                    wait[i].release();
                }
            }
            accessTable.release();
        }

        @Override
        public void run() {
            super.run();
        }
    }

    public static class Smoker extends Thread {

        private int type;
        private Table table;

        public Smoker(int type, Table table) {
            this.type = type;
            this.table = table;
        }

        public void execute() throws InterruptedException {
            accessTable.acquire();

            if (this.table.hasMyItems(type)) {
                this.table.consume(type);
                emptyTable.release();
            } else {
                waiting[type] = true;
                accessTable.release();
                wait[type].acquire();
            }
        }

        @Override
        public void run() {
            super.run();
        }
    }
}
