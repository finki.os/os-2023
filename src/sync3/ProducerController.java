package sync3;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class ProducerController {

    public static int NUM_RUN = 50;

    static Semaphore canAccessBuffer;
    static Semaphore canControl;
    static Semaphore lock;
    static int numControllers = 0;

    public static void init() {
        canAccessBuffer = new Semaphore(1);
        canControl = new Semaphore(10);
        lock = new Semaphore(1);
    }

    public static class Buffer {

        public void produce() {
            System.out.println("Producer is producing...");
        }

        public void check() {
            System.out.println("Controller is checking...");
        }
    }

    public static class Producer extends Thread {
        private final Buffer buffer;

        public Producer(Buffer b) {
            this.buffer = b;
        }

        public void execute() throws InterruptedException {
            canAccessBuffer.acquire();
            this.buffer.produce();
            canAccessBuffer.release();
        }

        @Override
        public void run() {
            for (int i = 0; i < NUM_RUN; i++) {
                try {
                    execute();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static class Controller extends Thread {

        private final Buffer buffer;

        public Controller(Buffer buffer) {
            this.buffer = buffer;
        }

        public void execute() throws InterruptedException {
            lock.acquire();
            if (numControllers == 0) {
                canAccessBuffer.acquire();
            }
            numControllers++;
            lock.release();

            canControl.acquire();
            this.buffer.check();

            lock.acquire();
            numControllers--;
            canControl.release();

            if (numControllers == 0) {
                canAccessBuffer.release();
            }
            lock.release();
        }

        @Override
        public void run() {
            for (int i = 0; i < NUM_RUN; i++) {
                try {
                    execute();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) {
        Buffer buffer = new Buffer();
        Producer p = new Producer(buffer);
        List<Controller> controllers = new ArrayList<>();
        init();
        for (int i = 0; i < 10; i++) {
            controllers.add(new Controller(buffer));
        }
        p.start();
        for (int i = 0; i < 10; i++) {
            controllers.get(i).start();
        }
    }
}
