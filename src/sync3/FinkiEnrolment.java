package sync3;

import java.util.concurrent.Semaphore;

public class FinkiEnrolment {

    static Semaphore manageMembers;
    static Semaphore canEnter;
    static Semaphore canEnrol;
    static Semaphore shouldLeave;

    public static void init() {
        manageMembers = new Semaphore(4);
        canEnter = new Semaphore(0);
        canEnrol = new Semaphore(0);
        shouldLeave = new Semaphore(0);
    }

    public static class Member extends Thread {

        public void execute() throws InterruptedException {
            manageMembers.acquire();
            int i = 10;

            while (i > 0) {
                canEnter.release();
                canEnrol.acquire();
                this.enrol();
                shouldLeave.release();
                i--;
            }
            manageMembers.release();
        }

        public void enrol() {
            System.out.println("Enrol student...");
        }

        @Override
        public void run() {
            try {
                execute();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static class Student extends Thread {

        public void execute() throws InterruptedException {
            canEnter.acquire();
            this.giveDocuments();
            canEnrol.release();
            shouldLeave.acquire();
        }

        public void giveDocuments() {
            System.out.println("Giving documents...");
        }

        @Override
        public void run() {
            try {
                execute();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

