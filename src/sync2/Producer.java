package sync2;

import java.util.concurrent.Semaphore;

public class Producer extends Thread {
    private Buffer buffer;

    public Producer(Buffer buffer) {
        this.buffer = buffer;
    }

    public void execute() throws InterruptedException {
        Locks.bufferEmpty.acquire();

        Locks.bufferLock.acquire();
        buffer.fillBuffer();
        Locks.bufferLock.release();

        for (Semaphore s : Locks.items) {
            s.release();
        }
    }

    @Override
    public void run() {
        for (int i = 0; i < Application.NUM_RUNS; i++) {
            try {
                execute();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
