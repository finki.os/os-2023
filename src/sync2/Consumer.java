package sync2;

public class Consumer extends Thread {
    private Buffer buffer;
    private int id;

    public Consumer(int id, Buffer buffer) {
        this.buffer = buffer;
        this.id = id;
    }

    public void execute() throws InterruptedException {
        Locks.items[id].acquire();
        this.buffer.getItem(id);

        Locks.bufferLock.acquire();
        this.buffer.decrementNumberOfItemsLeft();

        if (this.buffer.isBufferEmpty()) {
            Locks.bufferEmpty.release();
        }

        Locks.bufferLock.release();
    }

    @Override
    public void run() {
        for (int i = 0; i < Application.NUM_RUNS; i++) {
            try {
                execute();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
