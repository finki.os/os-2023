#include <stdio.h>
#include <sys/types.h>

int main(void){

	pid_t pid;
	pid = fork();

	switch(pid){

	case -1:
		fprintf(stderr,"fork failed\n");
		exit(2);

	case 0:
		printf("I'm the child\n");
		printf("My parrent is %d\n", getppid());
		printf("My PID is %d\n", getpid());

		sleep(2);
		exit(3);

	default:
		printf("I'm the parrent\n");
		printf("My PID is %d\n", getpid() );
		printf("My child's PID is %d\n", pid);
	}
}